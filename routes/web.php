<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['midlleware' => 'web', 'XSS'], function () {

	Route::get('/', 'HomeController@splash');
	Route::get('/home' , 'HomeController@index');
	Route::get('/berita', 'HomeController@berita');
	Route::get('/detail-berita/{slug}', 'HomeController@getBerita');
	Route::get('/profil-umum', 'HomeController@profilUmum');
	Route::get('/visi', 'HomeController@visi');
	Route::get('/ekstra', 'HomeController@ekstra');
	Route::get('/lsp', 'HomeController@lsp');
	Route::get('/jurusan/rpl', 'HomeController@rpl');
	Route::get('/jurusan/dkv', 'HomeController@dkv');
	Route::get('/jurusan/apk', 'HomeController@apk');
	Route::get('/jurusan/ak', 'HomeController@ak');
	Route::get('/jurusan/pbr', 'HomeController@pbr');
	Route::get('/event', 'HomeController@event');
	Route::get('/smeas-admin', 'AuthController@getLogin');
	Route::post('/smeas-admin', 'AuthController@doLogin');

});

Route::group(['middleware' => 'auth', 'XSS'], function () {

	Route::get('/smeas-admin/home', 'AdminController@index');
	Route::resource('/smeas-admin/slider', 'SliderController');
	Route::resource('/smeas-admin/gallery', 'GaleriController');
	Route::resource('/smeas-admin/berita', 'BeritaController');
	Route::resource('/smeas-admin/agenda', 'AgendaController');
	Route::resource('/smeas-admin/link', 'LinkController');
	Route::resource('/smeas-admin/produk', 'ProdukController');
	// Route::resource('/smeas-admin/setting', 'SettingController');
	Route::get('/smeas-admin/profile', 'AdminController@pageProfile');
	Route::post('/smeas-admin/profiles', 'AdminController@ubahPassword');
	Route::post('/smeas-admin/profile', 'AdminController@ubahProfile');
	Route::get('/smeas-admin/logout', 'AuthController@doLogout');
});