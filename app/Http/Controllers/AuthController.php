<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Tracker;
use Alert;
use Auth;
use Validator;

/**
* Created By LuwakDev
*/

class AuthController extends Controller
{
	
	function __construct()
	{
		# code...
		Tracker::hit();
	}

	public function getLogin()
	{
		return view('admin.login');
	}

	public function doLogin(Request $request)
	{
		$valid = Validator::make(
				$request->all(), array(
					'username' => 'required',
					'password' => 'required'
					));

		if ($valid->passes()) {
			# code...
			if(Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
				Alert::success('Selamat Anda Berhasil Login', 'Success', 'Success');
				return redirect('/smeas-admin/home');
			} else {
				Alert::error('Username atau Password Salah', 'Error', 'Error');
				return redirect()->back()->withErrors($valid);
			}
		} else {
			Alert::info('Username atau Password Masih Kosong', 'Info', 'Info');
			return redirect()->back()->withErrors($valid);
		}
	}

	public function doLogout()
	{
		if (Auth::logout()) {
			# code...
			Alert::error('Logout Gagal', 'Error', 'Error');
			return redirect('/smeas-admin');
		} else {
			Alert::success('Logout Berhasil', 'Success', 'Success');
			return redirect('/smeas-admin');
		}
	}
}