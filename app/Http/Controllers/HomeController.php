<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;
use App\Slider;
use App\Tracker;
use App\Link;
use App\Setting;
use App\Agenda;
use App\Produk;
use App\Galeri;
use Alert;
use DB;

class HomeController extends Controller
{
    //
    public function __construct()
    {
        Tracker::hit();
    }

    public function splash()
    {
        return view('welcome');
    }

    public function index()
    {
        $slider = Slider::where('status', '=', 'y')->get();
        $rand = Berita::inRandomOrder()->take(4)->get();
        $link = Link::take(4)->get();
    	return view('index', ['rand' => $rand, 'slider' => $slider, 'link' => $link]);
    }

    public function berita()
    {
        $data = Berita::paginate(4);
        $rand = Berita::inRandomOrder()->take(4)->get();
        $agenda = Agenda::inRandomOrder()->take(5)->get();
        $link = Link::take(4)->get();
        return view('berita', ['rand' => $rand, 'data' => $data, 'link' => $link, 'agenda' => $agenda]);
    }

    public function getBerita($slug)
    {
        $data = Berita::where('slug', '=', $slug)->get();
        $dibaca = DB::table('tb_berita')->where('slug', '=', $slug)->increment('dibaca', 1);
        $rand = Berita::inRandomOrder()->take(4)->get();
        $agenda = Agenda::inRandomOrder()->take(5)->get();

        return view('lihat-berita', ['data' => $data, 'rand' => $rand, 'agenda' => $agenda]);
    }

    public function profilUmum()
    {
        $rand = Berita::inRandomOrder()->take(4)->get();
    	return view('profil-umum', ['rand' => $rand]);
    }

    public function visi()
    {
        $rand = Berita::inRandomOrder()->take(4)->get();
        return view('visi', ['rand' => $rand]);
    }

    public function ekstra()
    {
        $rand = Berita::inRandomOrder()->take(4)->get();
        return view('ekstra', ['rand' => $rand]);
    }

    public function lsp()
    {
        $rand = Berita::inRandomOrder()->take(4)->get();
        return view('lsp', ['rand' => $rand]);
    }

    public function rpl()
    {
        $rand = Berita::inRandomOrder()->take(4)->get();
        $data = Produk::where('jurusan', '=', 'RPL')->get();
        $gal = Galeri::where('jurusan', '=', 'RPL')->get();
        return view('profil-rpl', ['data' => $data, 'rand' => $rand, 'gal' => $gal]);
    }

    public function ak()
    {
        $rand = Berita::inRandomOrder()->take(4)->get();
        $data = Produk::where('jurusan', '=', 'AK')->get();
        $gal = Galeri::where('jurusan', '=', 'AK')->get();
        return view('profil-ak', ['data' => $data, 'rand' => $rand, 'gal' => $gal]);
    }

    public function apk()
    {
        $rand = Berita::inRandomOrder()->take(4)->get();
        $data = Produk::where('jurusan', '=', 'APK')->get();
        $gal = Galeri::where('jurusan', '=', 'APK')->get();
        return view('profil-apk', ['data' => $data, 'rand' => $rand, 'gal' => $gal]);
    }

    public function dkv()
    {
        $rand = Berita::inRandomOrder()->take(4)->get();
        $data = Produk::where('jurusan', '=', 'DKV')->get();
        $gal = Galeri::where('jurusan', '=', 'DKV')->get();
        return view('profil-dkv', ['data' => $data, 'rand' => $rand, 'gal' => $gal]);
    }

    public function pbr()
    {
        $rand = Berita::inRandomOrder()->take(4)->get();
        $data = Produk::where('jurusan', '=', 'PBR')->get();
        $gal = Galeri::where('jurusan', '=', 'PBR')->get();
        return view('profil-pbr', ['data' => $data, 'rand' => $rand, 'gal' => $gal]);
    }

    public function event()
    {
        $rand = Berita::inRandomOrder()->take(4)->get();
        $data = Agenda::paginate(5);
        return view('event', ['data' => $data, 'rand' => $rand]);
    }
}
