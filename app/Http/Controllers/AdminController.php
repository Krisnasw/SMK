<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use Alert;
use App\User;
use App\Berita;
use App\Agenda;
use App\Galeri;
use App\Tracker;
use Hash;
use File;
use DB;

class AdminController extends Controller
{
    //
    function __construct()
    {
    	$this->middleware('auth');
        Tracker::hit();
    }

    public function index()
    {
        $trackers = Tracker::all();
        $trak = Tracker::count();
        $news = Berita::count();
        $poto = Galeri::count();
        $event = Agenda::count();

    	return view('admin.index', ['news' => $news, 'poto' => $poto, 'event' => $event, 'trackers' => $trackers, 'trak' => $trak]);
    }

    public function pageProfile()
    {
    	return view('admin.profile.index');
    }

    public function ubahProfile(Request $request)
    {
        $valid = Validator::make(
                $request->all(), array(
                    'image' => 'image|mimes:jpg,jpeg,png'
                    ));

        $data = User::findOrFail(Auth::user()->id);

        if ($valid->passes()) {
            # code...
            if ($request->hasFile('image')) {
                # code...
                $data['name'] = $request->name;
                $data['image'] = $this->deletePhoto($data['image']);
                $data['image'] = $this->savePhoto($request->file('image'));
                $data['image'] = $data['image'];
                $data->save();

                if ($data) {
                    # code...
                    Alert::success('Profile Berhasil Diubah', 'Success', 'Success');
                    return redirect()->back();
                } else {
                    Alert::error('Profile Gagal Diubah', 'Error', 'Error');
                    return redirect()->back();
                }
            } else {
                $data['name'] = $request->name;
                $data->save();

                if ($data) {
                    # code...
                    Alert::success('Profile Berhasil Diubah', 'Success', 'Success');
                    return redirect()->back();
                } else {
                    Alert::error('Profile Gagal Diubah', 'Error', 'Error');
                    return redirect()->back();
                }
            }
        } else {
            Alert::info('Data Yang Anda Isi Tidak Lengkap', 'Info', 'Info');
            return redirect()->back();
        }
    }

    public function ubahPassword(Request $request)
    {
        $lama = $request->pwlama;
        $baru = $request->pwbaru;
        $cbaru = $request->cpwbaru;

        $valid = Validator::make(
                $request->all(), array(
                    'pwlama' => 'required',
                    'pwbaru' => 'required',
                    'cpwbaru' => 'required'
                    ));

        if (Hash::check($lama, Auth::user()->password)) {
            # code...
            if ($baru == $cbaru) {
                # code...
                $data = DB::table('users')
                        ->where('id', '=', Auth::user()->id)
                        ->update(array('password' => Hash::make($cbaru)));

                    if ($data) {
                        # code...
                        Alert::success('Password Berhasil Diubah', 'Success', 'Success');
                        return redirect()->back();
                    } else {
                        # code...
                        Alert::error('Password Gagal Diubah', 'Error', 'Error');
                        return redirect()->back();
                    }
            } else {
                Alert::info('Password Baru Tidak Cocok', 'Info', 'Info');
                return redirect()->back();
            }
        } else {
            Alert::info('Password Lama Tidak Cocok', 'Info', 'Info');
            return redirect()->back();
        }
    }

    protected function savePhoto($photo)
    {
        $destinationPath = 'images';
        $subdestinationPath = 'user';
        $extension = $photo->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $photo->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $data['image'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;

        return $data['image'];
    }

    protected function deletePhoto($photo)
    {
        File::delete($photo);
        return $photo;
    }
}
