<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\Setting;
use Alert;
use File;
use Validator;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Produk::all();
        $jur = Setting::select('slug', 'nama_jurusan')->get();
        return view('admin.produk.index', ['data' => $data, 'jur' => $jur]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make(
            $request->all(), array(
                'image' => 'image|mimes:jpg,jpeg,png',
                'jurusan' => 'required',
                'keterangan' => 'required'
                ));

        $data = new Produk();

        if ($valid->passes()) {
            # code...
            $data['image'] = $this->savePhoto($request->file('image'));
            $data['image'] = $data['image'];
            $data['caption'] = $request->keterangan;
            $data['slug'] = str_slug($request->keterangan);
            $data['jurusan'] = $request->jurusan;
            $data->save();

            if ($data) {
                # code...
                Alert::success("Produk Berhasil Ditambah", "Success", "Success");
                return redirect()->back();
            } else {
                Alert::error("Gagal Tambah Produk", "Error", "Error");
                return redirect()->back();
            }
        } else {
            Alert::info("Data Yang Anda Isi Tidak Lengkap", "Info", "Info");
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make(
            $request->all(), array(
                'image' => 'image|mimes:jpg,jpeg,png'
                ));

        $data = Produk::findOrFail($id);

        if ($valid->passes()) {
            # code...
            $data['image'] = $this->deletePhoto($data['image']);
            $data['image'] = $this->savePhoto($request->file('image'));
            $data['image'] = $data['image'];
            $data['caption'] = $request->caption;
            $data['slug'] = str_slug($request->caption);
            $data['jurusan'] = $request->jurusan;
            $data->save();

            if ($data) {
                # code...
                Alert::success("Produk Berhasil Diupdate", "Success", "Success");
                return redirect()->back();
            } else {
                Alert::error("Produk Gagal Diupdate", "Error", "Error");
                return redirect()->back();
            }
        } else {
            Alert::info("Data Yang Anda Isi Kurang Lengkap", "Info", "Info");
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = Produk::findOrFail($id);
        $deleteImage = $this->deletePhoto($del['image']);
        $del->delete();

        if ($del) {
            # code...
            Alert::success("Produk Berhasil Dihapus", "Success", "Success");
            return redirect()->back();
        } else {
            Alert::error("Produk Gagal Dihapus", "Error", "Error");
            return redirect()->back();
        }
    }

    protected function savePhoto($photo)
    {
        $destinationPath = 'images';
        $subdestinationPath = 'produk';
        $extension = $photo->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $photo->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $data['image'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;

        return $data['image'];
    }

    protected function deletePhoto($photo)
    {
        File::delete($photo);
        return $photo;
    }
}
