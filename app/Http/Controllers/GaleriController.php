<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Galeri;
use App\Tracker;
use App\Setting;
use Alert;
use File;
use Validator;

class GaleriController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
        Tracker::hit();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Galeri::all();
        $jur = Setting::select('slug', 'nama_jurusan')->get();
        return view('admin.galeri.index', ['data' => $data, 'jur' => $jur]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make(
                $request->all(), array(
                    'image' => 'image|mimes:jpg,jpeg,png|required',
                    'keterangan' => 'required',
                    'jurusan' => 'required'
                    ));

        if ($valid->passes()) {
            # code...
            if ($request->hasFile('image')) {
                # code...
                $data = new Galeri();
                $data['foto_jurusan'] = $this->savePhoto($request->file('image'));
                $data['foto_jurusan'] = $data['foto_jurusan'];
                $data['keterangan'] = $request->keterangan;
                $data['jurusan'] = $request->jurusan;
                $data->save();

                if ($data) {
                    # code...
                    Alert::success('Data Berhasil Ditambahkan', 'Success', 'Success');
                    return redirect()->back()->withErrors($valid);
                } else {
                    Alert::info('Data Yang Anda Isi Salah', 'Info', 'Info');
                    return redirect()->back()->withErrors($valid);
                }
            } else {
                Alert::info('Gambar Tidak Boleh Kosong', 'Info', 'Info');
                return redirect()->back();
            }
        } else {
            Alert::error('Data Yang Anda Isi Tidak Lengkap', 'Error', 'Error');
            return redirect()->back()->withErrors($valid);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make(
            $request->all(), array(
                'image' => 'image|mimes:jpg,jpeg,png'
                ));

        $data = Galeri::findOrFail($id);

        if ($valid->passes()) {
            # code...
            if ($request->hasFile('image')) {
                # code...
                $data['foto_jurusan'] = $this->deletePhoto($data['foto_jurusan']);
                $data['foto_jurusan'] = $this->savePhoto($request->file('image'));
                $data['foto_jurusan'] = $data['foto_jurusan'];
                $data['keterangan'] = $request->keterangan;
                $data['jurusan'] = $request->jurusan;
                $data->save();

                if ($data) {
                    # code...
                    Alert::success('Sukses Update Data', 'Success', 'Success');
                    return redirect()->back();
                } else {
                    Alert::error('Gagal Update Data', 'Error', 'Error');
                    return redirect()->back()->withErrors($valid);
                }
            } else {
                $data['keterangan'] = $request->keterangan;
                $data['jurusan'] = $request->jurusan;
                $data->save();

                if ($data) {
                    # code...
                    Alert::success('Sukses Update Data', 'Success', 'Success');
                    return redirect()->back();
                } else {
                    Alert::error('Gagal Update Data', 'Error', 'Error');
                    return redirect()->back()->withErrors($valid);
                }
            }
        } else {
            Alert::error('Data Yang Anda Isi Tidak Lengkap', 'Error', 'Error');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = Galeri::findOrFail($id);
        $deleteImage = $this->deletePhoto($del['foto_jurusan']);
        $del->delete();

        if ($del) {
            # code...
            Alert::success('Galeri Berhasil Dihapus', 'Success', 'Success');
            return redirect()->back();
        } else {
            Alert::error('Gagal Hapus Galeri', 'Error', 'Error');
            return redirect()->back();
        }
    }

    protected function savePhoto($photo)
    {
        $destinationPath = 'images';
        $subdestinationPath = 'galeri';
        $extension = $photo->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $photo->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $data['foto_jurusan'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;

        return $data['foto_jurusan'];
    }

    protected function deletePhoto($photo)
    {
        File::delete($photo);
        return $photo;
    }
}
