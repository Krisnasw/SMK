<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;
use Alert;
use Validator;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Setting::all();
        return view('admin.jurusan.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make(
                $request->all(), array(
                    'nama' => 'required',
                    'pengertian' => 'required',
                    'image' => 'image|mimes:jpg,jpeg,png',
                    'profesi' => 'required',
                    'kompetensi' => 'required'
                    ));

        $data = new Setting();

        if ($valid->passes()) {
            # code...
            if ($request->hasFile('image')) {
                # code...
                $data['logo'] = $this->savePhoto($request->file('image'));
                $data['logo'] = $data['logo'];
                $data['nama_jurusan'] = $request->nama;
                $data['keterangan'] = $request->pengertian;
                $data['slug'] = str_slug($request->nama);
                $data['kompetensi'] = $request->kompetensi;
                $data['profesi'] = $request->profesi;
                $data['prestasi'] = $request->prestasi;
                $data->save();

                if ($data) {
                    # code...
                    Alert::success('Jurusan Berhasil Ditambah', 'Success!', 'Success');
                    return redirect()->back();
                } else {
                    Alert::error('Gagal Tambah Jurusan', 'Error!', 'Error');
                    return redirect()->back();
                }
            } else {
                $data['nama_jurusan'] = $request->nama;
                $data['keterangan'] = $request->pengertian;
                $data['slug'] = str_slug($request->nama);
                $data['kompetensi'] = $request->kompetensi;
                $data['profesi'] = $request->profesi;
                $data['prestasi'] = $request->prestasi;
                $data->save();

                if ($data) {
                    # code...
                    Alert::success('Jurusan Berhasil Ditambah', 'Success!', 'Success');
                    return redirect()->back();
                } else {
                    Alert::error('Gagal Tambah Jurusan', 'Error!', 'Error');
                    return redirect()->back();
                }
            }
        } else {
            Alert::info('Data Tidak Lengkap', 'Info!', 'Info');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = Setting::findOrFail($id);
        $deleteImage = $this->deletePhoto($del['logo']);
        $del->delete();

        if ($del) {
            # code...
            Alert::success('Jurusan Berhasil Dihapus', 'Success!', 'Success');
            return redirect()->back();
        } else {
            Alert::error('Gagal Hapus Jurusan', 'Error!', 'Error');
            return redirect()->back();
        }
    }

    protected function savePhoto($photo)
    {
        $destinationPath = 'images';
        $subdestinationPath = 'jurusan';
        $extension = $photo->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $photo->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $data['logo'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;

        return $data['logo'];
    }

    protected function deletePhoto($photo)
    {
        File::delete($photo);
        return $photo;
    }
}
