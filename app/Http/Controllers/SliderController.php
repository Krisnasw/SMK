<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Slider;
use App\Tracker;
use Alert;
use File;

class SliderController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
        Tracker::hit();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Slider::all();

        return view('admin.slider.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make(
                $request->all(), array(
                    'image' => 'image|mimes:jpg,png,jpeg|required',
                    'keterangan' => 'min:3'
                    ));

        if ($valid->passes()) {
            # code...
            $data = new Slider();
            $data['image'] = $this->savePhoto($request->file('image'));
            $data['image'] = $data['image'];
            $data['keterangan'] = $request->keterangan;
            $data['slug'] = str_slug($request->keterangan);
            $data['status'] = "y";
            $data->save();

            if ($data) {
                # code...
                Alert::success('Data Berhasil Ditambahkan', 'Success', 'Success');
                return redirect()->back()->withErrors($valid);
            } else {
                Alert::info('Data Yang Anda Isi Salah', 'Info', 'Info');
                return redirect()->back()->withErrors($valid);
            }
        } else {
            Alert::error('Data Yang Anda Isi Tidak Lengkap', 'Error', 'Error');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make(
            $request->all(), array(
                'image' => 'image|mimes:jpg,jpeg,png'
                ));

        $data = Slider::findOrFail($id);

        if ($valid->passes()) {
            # code...
            if ($request->hasFile('image')) {
                # code...
                $data['image'] = $this->deletePhoto($data['image']);
                $data['image'] = $this->savePhoto($request->file('image'));
                $data['image'] = $data['image'];
                $data['keterangan'] = $request->keterangan;
                $data['slug'] = str_slug($request->keterangan);
                $data['status'] = $request->status;
                $data->save();

                if ($data) {
                    # code...
                    Alert::success('Sukses Update Data', 'Success', 'Success');
                    return redirect()->back();
                } else {
                    Alert::error('Gagal Update Data', 'Error', 'Error');
                    return redirect()->back()->withErrors($valid);
                }
            } else {
                $data['keterangan'] = $request->keterangan;
                $data['slug'] = str_slug($request->keterangan);
                $data['status'] = $request->status;
                $data->save();

                if ($data) {
                    # code...
                    Alert::success('Sukses Update Data', 'Success', 'Success');
                    return redirect()->back();
                } else {
                    Alert::error('Gagal Update Data', 'Error', 'Error');
                    return redirect()->back()->withErrors($valid);
                }
            }
        } else {
            Alert::error('Data Yang Anda Isi Tidak Lengkap', 'Error', 'Error');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = Slider::findOrFail($id);
        $deleteImage = $this->deletePhoto($del['image']);
        $del->delete();

        if ($del) {
            # code...
            Alert::success('Data Berhasil Dihapus', 'Success', 'Success');
            return redirect()->back();
        } else {
            Alert::error('Gagal Hapus Data', 'Error', 'Error');
            return redirect()->back();
        }
    }

    protected function savePhoto($photo)
    {
        $destinationPath = 'images';
        $subdestinationPath = 'slider';
        $extension = $photo->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $photo->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $data['image'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;

        return $data['image'];
    }

    protected function deletePhoto($photo)
    {
        File::delete($photo);
        return $photo;
    }
}
