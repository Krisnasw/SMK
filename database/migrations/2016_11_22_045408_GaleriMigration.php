<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GaleriMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tb_galeri', function (Blueprint $table) {
            $table->increments('id');
            $table->text('keterangan');
            $table->string('foto_jurusan');
            $table->enum('jurusan', ['AK','APK','PBR','APH','RPL','MM','DKV','BC','TKJ']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('tb_galeri');
    }
}
