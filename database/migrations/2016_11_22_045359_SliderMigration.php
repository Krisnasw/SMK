<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SliderMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tb_slider', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image');
            $table->text('keterangan');
            $table->string('slug');
            $table->enum('status', ['y', 'n']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('tb_slider');
    }
}
