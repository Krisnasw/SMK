@include('header')
<!-- .nav -->
<section class="single-page-title">
    <div class="container text-center">
        <h2>Agenda Kegiatan</h2>
    </div>
</section>
<!-- .page-title -->

<section class="about-text ptb-100">
    <section class="section-title">
        <div class="container text-center">
        </div>
    </section>

<div class="container">
    <div class="row">
        <div class="col-md-12">
    <h3 style="border-left: 3px solid #1384d3; padding: 10px; text-align: left;"><i class="fa fa-calendar">&nbsp;</i>Agenda Kegiatan SMK Negeri 1 Surabaya</h3><br>
                                        <div class="panel-group acc-v1" id="accordion-1">
                                        @foreach($data as $key)
                                            <div class="panel panel-default">
                                                <div class="panel-heading bg-panel">
                                                        <h4 class="panel-title">
                                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-One{{{ $key['id'] }}}">
                                                                        {!! $key['judul'] !!}
                                                                </a>
                                                        </h4>
                                                </div>
                                                <div id="collapse-One{{{ $key['id'] }}}" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                                <div class="row">
                                                                        <div class="col-md-4">
                                                                                <img class="img-responsive" src="{{ asset($key['image']) }}" alt="">
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                        <i class="fa fa-calendar fa-2x">&nbsp;&nbsp;</i><span style="font-size: 15px;">Dimulai : {!! $key['tgl_mulai'] !!}</span><hr>
                                                                        <i class="fa fa-clock-o fa-2x">&nbsp;&nbsp;</i><span style="font-size: 15px;">Selesai : {!! $key['tgl_selesai'] !!}</span><hr>
                                                                        <i class="fa fa-map-marker fa-2x">&nbsp;&nbsp;</i><span style="font-size: 15px;">{!! $key['isi'] !!}</span><hr>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                </div>
                                            </div>
                                        @endforeach
                                <!-- End Accordion v1 -->
                                        </div>
        </div>
    </div>
    <ul class="pagination" style="margin-left: 42%;">
      {!! $data->links() !!}
    </ul>
</div>

</section>
<!-- .about-text-->

<!-- #x-corp-carousel-->
<section class="x-services ptb-100 gray-bg">

    <section class="section-title">
        <div class="container text-center">
            <h2>Berita Terbaru</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
        @foreach($rand as $key)
           <div class="col-md-3 col-sm-6">
                <div class="thumbnails thumbnail-style thumbnail-kenburn">
                    <div class="thumbnail-img">
                        <div class="overflow-hidden">
                            <img class="img-responsive" src="{{ asset($key['image']) }}" alt="">
                        </div>
                        <a class="btn-more hover-effect" href="{{ url('/detail-berita') }}/{{ $key['slug'] }}">Lihat Selengkapnya</a>
                    </div>
                    <div class="caption">
                        <h3><a class="hover-effect" href="{{ url('/detail-berita') }}/{{ $key['slug'] }}">{!! $key['judul'] !!}</a></h3>
                        <p>
                            {!! substr($key['isi'], 0,70) !!}
                        </p>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
        <!-- .row -->
    <!-- .container -->
    <!-- .container -->
</section>
<!-- .x-services -->
@include('footer')