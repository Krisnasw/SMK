
<div class="foot-info">
    <div class="col-md-4 col-sm-12 col-xs-12">
        <h3>Tentang Kami</h3>
        <img src="{{ asset('assets/img/logo3.png') }}">
        <p>Sekolah Kejuruan di Surabaya, Jawa Timur yang berlokasi di JL. SMEA NO. 4,  WONOKROMO SURABAYA, SMK NEGERI 1 SURABAYA bertekad mencapai perbaikan yang berkesinambungan berdasarkan sistem manajemen mutu ISO 9001:2000. </p>
    </div>
    <div class="col-md-4 col-sm-12 col-xs-12">
       <h3>Link Terkait</h3>
       <p><a href="{{ url('/home') }}" title="">Home</a></p>
       <p><a href="{{ url('/profil-umum') }}" title="">Profil Sekolah</a></p>
       <p><a href="#" title="Sejarah Pengembangan">Sejarah Pengembangan </a></p>
       <p><a href="http://bkk-smkn1sby.com" title="BKK">BKK</a></p>
    </div>
    <div class="col-md-4 col-sm-12 col-xs-12">
        <h3>Info Kontak</h3>
        <p><i class="fa fa-map-marker">&nbsp;</i>Alamat : <br>JL. SMEA No. 4,  Wonokromo Surabaya, Jawa Timur</p>
        <p><i class="fa fa-phone">&nbsp;</i>Telepon : <br>031-8292038</p>
        <p><i class="fa fa-envelope">&nbsp;</i>Mail : <br> <a href="mailto:info@smkn-1.sch.id">info@smkn-1.sch.id</a></p>
    </div>
</div>
<footer class="footer">
    <!-- Footer Widget Section -->
    <div class="copyright-section">
        <div class="container clearfix">
                <span class="pull-left">Copyright &copy; 2016 | <a href="https://luwakdev.id"> <strong style="color:#31aae2;">Luwakdev</strong></a> </span>

            <ul class="list-inline pull-right">
                <li class="active"><a href="{{ url('/home') }}">Beranda</a></li>
                <li><a href="{{ url('/profil-umum') }}">Profil</a></li>
                <li><a href="{{ url('/berita') }}">Berita</a></li>
                <li><a href="{{ url('/event') }}">Event</a></li>
            </ul>
        </div><!-- .container -->
    </div><!-- .copyright-section -->
</footer>
<!-- .footer -->

</div>
<!-- .content-wrapper -->
</div>
<!-- .offcanvas-pusher -->

<div class="uc-mobile-menu uc-mobile-menu-effect">
    <button type="button" class="close" aria-hidden="true" data-toggle="offcanvas"
            id="uc-mobile-menu-close-btn">&times;</button>
    <div>
        <div>
            <ul id="menu">
                <li><a href="{{ url('/home') }}">Beranda</a></li>
                <li><a href="profil-umum.html">Profil Sekolah</a>
                <li><a href="berita.html">Berita</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- .uc-mobile-menu -->

</div>
<!-- #main-wrapper -->

<!-- Modal Image -->
<div id="myModal" class="modal">

  <span class="close" onclick="document.getElementById('myModal').style.display='none'">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>
<!-- Modal Image -->

<!-- Script -->
{!! Html::script('assets/js/jquery-2.1.4.min.js') !!}
{!! Html::script('assets/js/bootstrap.min.js') !!}
{!! Html::script('assets/js/smoothscroll.js') !!}
{!! Html::script('assets/js/mobile-menu.js') !!}
{{-- {!! Html::script('assets/js/function.js') !!} --}}
{!! Html::script('assets/js/wow.min.js') !!}
{!! Html::script('assets/js/owl.carousel.min.js') !!}
{!! Html::script('assets/js/nivo-lightbox.min.js') !!}
{!! Html::script('assets/js/custom.js') !!}
{!! Html::script('assets/plugins/fancybox/source/jquery.fancybox.pack.js') !!}
{!! Html::script('assets/js/fancy-box.js') !!}
{!! Html::script('assets/js/flexSlider/jquery.flexslider-min.js') !!}
{!! Html::script('assets/js/scripts.js') !!}
{!! Html::script('assets/js/jquery.particleground.js') !!}

                <script>
                    // Get the modal
                var modal = document.getElementById('myModal');

                var img = document.getElementById('myImg');
                var modalImg = document.getElementById("img01");
                var captionText = document.getElementById("caption");
                img.onclick = function(){
                    modal.style.display = "block";
                    modalImg.src = this.src;
                    captionText.innerHTML = this.alt;
                }

                var span = document.getElementsByClassName("close")[0];

                span.onclick = function() { 
                  modal.style.display = "none";
                }
                </script>
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        FancyBox.initFancybox();
                    });
                </script>
                <script type="text/javascript">
                $(document).ready(function() {

                  $("#x-corp-carousel .carousel-inner .item:first").addClass("active");
                 
                });
                </script>
                <script id="dsq-count-scr" src="//smkn-1-surabaya-1.disqus.com/count.js" async></script>
                <script>
                /**
                *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                /*
                var disqus_config = function () {
                this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                };
                */
                (function() { // DON'T EDIT BELOW THIS LINE
                var d = document, s = d.createElement('script');
                s.src = '//smkn-1-surabaya-1.disqus.com/embed.js';
                s.setAttribute('data-timestamp', +new Date());
                (d.head || d.body).appendChild(s);
                })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
            <div/>
        </div>
    </body>
</html>