@include('header')

<section class="single-page-title">
    <div class="container text-center">
        <h2>Lembaga Sertifikasi Profesi</h2>
    </div>
</section>
<!-- .page-title -->

<section class="about-text ptb-100">
    <section class="section-title">
        <div class="container text-center">
        </div>
    </section>
<div class="col-md-12">
<div class="seocips-tbn-frm" style="margin-bottom: 5%;">
 <div class="seocips-tbn-frm_inner bottomshadows">
<iframe allowfullscreen="" frameborder="0" height="270" src="video/lsp-final.mp4?autoplay=0" width="480" ></iframe>
 </div>
</div>
</div>
    <div class="container">
        <div class="row">
           <div class="lsp">
           <div class="col-md-4">
           <img class="animated bounceIn" src="assets/img/lsp/lsp.png" alt="">
           </div>
           <div class="col-md-8">
               <h3>Lembaga Sertifikasi Profesi (LSP)</h3><br>
               <p>Lembaga sertifikasi profesi adalah lembaga pendukung BNSP yang bertanggung jawab melaksanakan sertifikasi kompetensi profesi. LSP SMK Negeri 1 Surabaya yang dibentuk berbadan hukum dan dibentuk oleh Lembaga Pendidikan dan Pelatihan  yang diregistrasi oleh BNSP.
LSP SMK Negeri 1 Surabaya mempunyai tugas mengembangkan standar kompetensi, melaksanakan uji kompetensi, menerbitkan sertifikat kompetensi serta melakukan verifikasi tempat uji kompetensi.<br><br>
Dalam melaksanakan tugas dan fungsi, LSP SMK Negeri 1 Surabaya mengacu pada pedoman yang dikeluarkan oleh BNSP. Dalam pedoman tersebut ditetapkan persyaratan yang harus dipatuhi untuk menjamin agar lembaga sertifikasi menjalankan sistem sertifikasi pihak pertama secara konsisten dan profesional, sehingga dapat diterima di tingkat nasional yang relevan demi kepentingan pengembangan sumber daya manusia dalam aspek peningkatan kualitas dan perlindungan tenaga kerja.<br>

           </div>
           </div>

        </div><!--row-->
    <div class="container">
    <hr>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-6">
            <div class="visi-lsp">
                        <h2><b>Visi</b></h2>
                        <p>Menjadi lembaga sertifikasi profesi yang menjamin kompetensi  tenaga kerja yang mampu bersaing, professional, berakhlak mulia dan berwawasan lingkungan.</p>
                    </div>
        </div>
        <div class="col-lg-6">
            <div class="visi-lsp">
                        <h2><b>Misi</b></h2>
                        <ol>
                            <li>&nbsp;LSP SMK Negeri 1 Surabaya menjamin menerapkan pedoman BNSP tahun 2014 dan  ISO 17024 </li>
                            <li>&nbsp;LSP SMK Negeri 1 Surabaya siap melaksanakan sertifikasi kompetensi secara profesional dan mampu bersaing.</li>
                            <li>&nbsp;Memberikan pelayanan uji  sertifikasi kompetensi yang mengutamakan mutu dan kepuasan pelanggan </li>
                            <li>&nbsp;Menjadi LSP yang menjamin ketidakperpihakan ,kejujuran, kedisiplinan dan berwawasan lingkungan.</li>
                            <li>&nbsp;Menghasilkan tenaga kerja  bersertifikat kompetensi yang profesional dan berakhlak mulia.</li>
                        </ol>
                    </div>
        </div>
                <div class="col-md-12">
                <hr>
                <div class="col-md-6">
                    <div class="visi-lsp">
                        <h2><b>Kebijakan Mutu</b></h2>
                         <p>LSP SMKN 1 Surabaya bertekad dapat mensertifikasi semua siswa sehingga menjadi tenaga kerja yang berkualifikasi sebagai berikut :</p>
                        <ol>

                            <li>&nbsp;Standart mutu ASEAN</li>
                            <li>&nbsp;Memelihara sertifikat kompetensi keahliannya</li>
                            <li>&nbsp;Kompeten dan konsisten dalam bekerja</li>
                            <li>&nbsp;Sikap disiplin dan budi pekerti luhur</li>
                            <li>&nbsp;Aktif, kreatif dan inovatif</li>
                            <li>&nbsp;Terampil menggunakan teknologi</li>
                            <li>&nbsp;Unggul dalam prestasi</li>
                        </ol>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="visi-lsp">
                        <h2><b>Formulir Permohonan Sertifikasi Kompetensi</b></h2>
                         <p>Apabila ingin mengajukan permohonan Sertifikasi Kompetensi silahkan mendownload file dibawah ini. isi formulir pendaftaran dengan benar lalu kirimkan ke : <br><i class="fa fa-envelope">&nbsp;</i><a href="mailto:lsp1.smkn1sby@gmail.com" title="">lsp1.smkn1sby@gmail.com</a>
                         <br><br>
                         <button type="submit" class="btn-download"><a style="color: #fff;" href="file/PermohonanSertifikasiKompetensi.docx" title=""><i class="fa fa-download" style="color: #fff;">&nbsp;</i>Download File</a></button>
                         
                    </div>
                </div>
                </div> 
                    
        </div>
    </div>
    </div>
    <section id="gallery" class="home-section text-center bg-gray">
<hr>
            <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="" data-wow-delay="0.4s">
                    <div class="section-heading">
                        <h2>FOTO PELAKSANAAN LSP</h2>

                    </div>
                    </div>
                </div>
            </div>
            </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12" >
                    <div class="wow">
                    <div id="owl-works" class="owl-carousel">
                        <div class="item2"><a href="assets/img/lsp/1.jpg" title="This is an image title" data-lightbox-gallery="gallery1"><img src="assets/img/lsp/1.jpg" class="img-responsive" alt="img"></a></div>
                        <div class="item2"><a href="assets/img/lsp/2.jpg" title="This is an image title" data-lightbox-gallery="gallery1"><img src="assets/img/lsp/2.jpg" class="img-responsive " alt="img"></a></div>
                        <div class="item2"><a href="assets/img/lsp/3.jpg" title="This is an image title" data-lightbox-gallery="gallery1"><img src="assets/img/lsp/3.jpg" class="img-responsive " alt="img"></a></div>
                        <div class="item2"><a href="assets/img/lsp/4.jpg" title="This is an image title" data-lightbox-gallery="gallery1"><img src="assets/img/lsp/4.jpg" class="img-responsive " alt="img"></a></div>
                        <div class="item2"><a href="assets/img/lsp/5.jpg" title="This is an image title" data-lightbox-gallery="gallery1"><img src="assets/img/lsp/5.jpg" class="img-responsive " alt="img"></a></div>
                        <div class="item2"><a href="assets/img/lsp/6.jpg" title="This is an image title" data-lightbox-gallery="gallery1"><img src="assets/img/lsp/6.jpg" class="img-responsive " alt="img"></a></div>

                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>
</section>
<!-- .about-text-->

<!-- #x-corp-carousel-->
<section class="x-services ptb-100 gray-bg">

    <section class="section-title">
        <div class="container text-center">
            <h2>Berita Terbaru</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
        @foreach($rand as $key)
           <div class="col-md-3 col-sm-6">
                <div class="thumbnails thumbnail-style thumbnail-kenburn">
                    <div class="thumbnail-img">
                        <div class="overflow-hidden">
                            <img class="img-responsive" src="{{ asset($key['image']) }}" alt="">
                        </div>
                        <a class="btn-more hover-effect" href="{{ url('/detail-berita') }}/{{ $key['slug'] }}">Lihat Selengkapnya</a>
                    </div>
                    <div class="caption">
                        <h3><a class="hover-effect" href="{{ url('/detail-berita') }}/{{ $key['slug'] }}">{!! $key['judul'] !!}</a></h3>
                        <p>
                            {!! substr($key['isi'], 0,70) !!}
                        </p>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
        <!-- .row -->
    <!-- .container -->
    <!-- .container -->
</section>
<!-- .x-services -->

@include('footer')