@include('header')

<section class="single-page-title">
    <div class="container text-center">
        <h2>Profil Pemasaran</h2>
    </div>
</section>
<!-- .page-title -->

<section class="about-text ptb-100">
    <section class="section-title">
        <div class="container text-center">
        </div>
    </section>
</section>
<div class="container">
<div class="row">
<div class="jurusan">
<div class="col-md-12">
<!-- <img src="assets/img/dkv/dkv.png" alt=""> --><hr>
    <!-- <div class="seocips-tbn-frm">
 <div class="seocips-tbn-frm_inner bottomshadows">
<iframe width="480" height="270" src="https://www.youtube.com/embed/zqYrZPpBAAE" frameborder="0" allowFullScreen=""></iframe>
 </div>
</div> -->
</div>
<div class="col-md-12">

<h2 style=" text-align:center; margin-top: 5%; padding: 1%;">Pemasaran (PBR)</h2><br>
<p>Merupakan Kristalisasi Dari Program Keahlian Yang Harus Dikuasai Oleh Peserta Didik Untuk Dapat Bekerja Sesuai Dengan Standart Kompetensi Kerja Nasional Indonesia (SKKNI) Atau pada Paket Keahlian Pemasaran.
Tujuan Program Keahlian Pemasaran secara umum mengacu pada isi Undang-Undang Sistem Pendidikan Nasional (UU SPN) Pasal 3 mengenai tujuan pendidikan nasional dan penjelasan Pasal 15 yang menyebutkan bahwa pendidikan kejuruan merupakan pendidikan menengah yang mempersiapkan peserta didik terutama untuk bekerja dalam bidang tertentu. Secara khusus tujuan Program Keahlian Pemasaran adalah membekali peserta didik dengan keterampilan, pengetahuan dan sikap agar kompeten di bidangnya.


</p>
</div>
</div>
</div>
<br><br>
<div class="row">
<div class="jurusan">
<div class="col-md-12"><hr><br>
<div class="col-md-6">
    <h3><i class="fa fa-book">&nbsp;</i>KOMPETENSI / MATERI YANG DIAJARKAN :</h3><br><br><br><br>
    <ol>
        <li>PENGANTAR EKONOMI BISNIS</li>
        <li>PENGANTAR ADMINISTRASI PERKANTORAN</li>
        <li>PENGANTAR AKUNTANSI</li>
        <li>SIMULASI DIGITAL</li>
        <li>ANALISA RISET PASAR</li>
        <li>PERENCANAAN PEMASARAN</li>
        <li>PENGELOLAAN USAHA PEMASARAN</li>
        <li>STRATEGI PEMASARAN</li>
        <li>PEMASARAN ON LINE</li>
        <li>PRINSIP-PRINSIP BISNIS</li>
        <li>PENGETAHUAN PRODUK</li>
        <li>PENATAAN BARANG DAGANGAN </li>
        <li>KOMUNIKASI BISNIS</li>
        <li>ADMINISTRASI BARANG</li>
        <li>ADMINISTRASI TRANSAKSI</li>
        <li>PELAYANAN PENJUALAN</li>
    </ol><br>
</div>
<div class="col-md-6">
    <h3><i class="fa fa-university">&nbsp;</i>PROFESI /BIDANG PEKERJAAN  :</h3><br><br><br><br>
    <ol>
        <li>PRAMUNIAGA</li>
        <li>CASIER</li>
        <li>DISPLAY</li>
        <li>LABELING</li>
        <li>DLL</li>
    </ol><br>
</div>
<div class="col-md-12">
<h3><i class="fa fa-trophy">&nbsp;</i>PRESTASI YANG DI DAPAT  :</h3><br><br><br><br>
    <ol>
        <li>JUARA 1 LKS KOTA SURABAYA TAHUN 2013</li>
        <li>JUARA 1 LKS PROVINSI JAWA TIMUR  TAHUN 2013</li>
        <li>JUARA 3 LKS NASIONAL TAHUN 2014</li>
        <li>JUARA 1 JAWARA KOTA SURABAYA TAHUN 2014</li>
        <li>JUARA 1 LKS KOTA SURABAYA TAHUN 2015</li>
        <li>JUARA 1 LKS PROVINSI JAWA TIMUR TAHUN 2015</li>
        <li>JUARA 2 LKS NASIONAL TAHUN 2015</li>
        <li>JUARA 1 LKS KOTA SURABAYA TAHUN 2016</li>
        <li>JUARA HARAPAN 1 LKS PROPINSI TAHUN 2016</li>
    </ol><br>
    </div>
</div>
</div>
</div>

</div>

 <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12" >
                <h3 style="border-left: 3px solid #ff7b00; padding: 10px;">Gallery Foto</h3>
                    <div class="wow">
                    <div id="owl-works" class="owl-carousel">
                    @foreach($gal as $key)
                        <div class="item2">
                            <a href="{{ asset($key['foto_jurusan']) }}" title="{!! $key['keterangan'] !!}" data-lightbox-gallery="gallery1">
                                <img src="{{ asset($key['foto_jurusan']) }}" class="img-responsive" alt="{!! $key['keterangan'] !!}">
                            </a>
                        </div>
                    @endforeach
                    </div>
                    </div>
                </div>
            </div>
        </div><br>
    <!-- Three Columns --><br>
    <div class="container content">
        <div class="text-center margin-bottom-50">
            <h3 style="border-left: 3px solid #ff7b00; padding: 10px; text-align: left;">Produk</h3>
        </div>

            <div class="row  margin-bottom-30">
                @foreach($data as $key)
                    <div class="col-sm-4 sm-margin-bottom-30">
                        <a href="{{ asset($key['image']) }}" rel="gallery1" class="fancybox img-hover-v1" title="{!! $key['caption'] !!}">
                            <span><img class="img-responsive" src="{{ asset($key['image']) }}" alt=""><p id="text-hover">{!! $key['caption'] !!}</p></span>
                        </a>
                    </div>
                @endforeach
            </div>

    </div>
    <!-- End Three Columns -->
<!-- #x-corp-carousel-->
<section class="x-services ptb-100 gray-bg">

    <section class="section-title">
        <div class="container text-center">
            <h2>Berita Terbaru</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
        @foreach($rand as $key)
           <div class="col-md-3 col-sm-6">
                <div class="thumbnails thumbnail-style thumbnail-kenburn">
                    <div class="thumbnail-img">
                        <div class="overflow-hidden">
                            <img class="img-responsive" src="{{ asset($key['image']) }}" alt="">
                        </div>
                        <a class="btn-more hover-effect" href="{{ url('/detail-berita') }}/{{ $key['slug'] }}">Lihat Selengkapnya</a>
                    </div>
                    <div class="caption">
                        <h3><a class="hover-effect" href="{{ url('/detail-berita') }}/{{ $key['slug'] }}">{!! $key['judul'] !!}</a></h3>
                        <p>{!! substr($key['isi'], 0,100) !!}</p>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
        <!-- .row -->
    <!-- .container -->
    <!-- .container -->
</section>

@include('footer')