<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="SMKN 1 Surabaya, SMK NEGERI 1 Surabaya">
    <meta author="LuwakDev">
    <title>SMK Negeri 1 Surabaya</title>
    <!-- web-fonts -->
    <link href='{{ asset('https://fonts.googleapis.com/css?family=Roboto:400,700,500') }}' rel='stylesheet' type='text/css'>
    <link href='{{ asset('http://fonts.googleapis.com/css?family=Montserrat:400,700') }}' rel='stylesheet' type='text/css'>
    <link href="{{ asset('assets/css/mobile-menu.css') }}" rel="stylesheet">
    <!-- font-awesome -->
    <link href="{{ asset('assets/fonts/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link href="{{ asset('assets/css/nivo-lightbox.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/nivo-lightbox-theme/default/default.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/owl.carousel.css') }}" rel="stylesheet" media="screen" />
    <link href="{{ asset('assets/css/owl.theme.css') }}" rel="stylesheet" media="screen" /> 
    <link rel="stylesheet" href="{{ asset('assets/plugins/fancybox/source/jquery.fancybox.css') }}">

    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet">
</head>
<body>
<div id="main-wrapper">
<!-- Page Preloader -->
<div id="preloader">
    <div id="status">
        <div class="status-mes"></div>
    </div>
</div>

<div class="uc-mobile-menu-pusher">

<div class="content-wrapper">
<nav class="navbar m-menu navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/home') }}"><img src="{{ asset('assets/img/logo4.png') }}" alt=""></a>
        </div>


        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="#navbar-collapse-1">

          
            <ul class="nav navbar-nav navbar-right main-nav">
                <li class="active"><a href="{{ url('/home') }}">Beranda</a></li>
                 <li class="dropdown "><a href="#" data-toggle="dropdown" class="dropdown-toggle">Profil
                    <span><i class="fa fa-angle-down"></i></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="m-menu-content">
                                
                                <ul>
                                    <li><a href="{{ url('/profil-umum') }}">Profil Umum</a></li>
                                    <li><a href="#">Sejarah Pengembangan</a></li>
                                    <li><a href="{{ url('/visi') }}">Visi dan Misi</a></li>
                                    <li><a href="#">Grafik Siswa Baru dan Pemetaan Lulusan</a></li>
                                    <li><a href="{{ url('/ekstra') }}">Osis dan Ekstrakurikuler</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>
                <li><a href="{{ url('/berita') }}">Berita</a></li>
                <li><a href="{{ url('/event') }}">Event</a></li>
                <li class="dropdown m-menu-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Kompetensi Keahlian
                    <span><i class="fa fa-angle-down"></i></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="m-menu-content">
                                <ul class="col-sm-4">
                                    <li class="dropdown-header" id="tag-sub">RPL ( Rekayasa Perangkat Lunak )</li>
                                    <li><a href="{{ url('/jurusan/rpl') }}">Profil Jurusan</a></li>
                                    <li><a href="#">Website Jurusan</a></li>
                                    <li class="dropdown-header" id="tag-sub">TKJ ( Teknik Komputer dan Jaringan)</li>
                                    <li><a href="{{ url('/jurusan/tkj') }}">Profil Jurusan</a></li>
                                    <li><a href="#">Website Jurusan</a></li>
                                    <li class="dropdown-header" id="tag-sub">MM ( Multimedia )</li>
                                    <li><a href="{{ url('/jurusan/mm') }}">Profil Jurusan</a></li>
                                    <li><a href="#">Website Jurusan</a></li>
                                </ul>
                                <ul class="col-sm-4">
                                    <li class="dropdown-header" id="tag-sub">DKV ( Desain Komunikasi Visual )</li>
                                    <li><a href="{{ url('/jurusan/dkv') }}">Profil Jurusan</a></li>
                                    <li><a href="#">Website Jurusan</a></li>
                                    <li class="dropdown-header" id="tag-sub">PBR ( Pemasaran )</li>
                                    <li><a href="{{ url('/jurusan/pbr') }}">Profil Jurusan</a></li>
                                    <li><a href="#">Website Jurusan</a></li>
                                    <li class="dropdown-header" id="tag-sub">TP3RP ( Broadcasting )</li>
                                    <li><a href="{{ url('/jurusan/bc') }}">Profil Jurusan</a></li>
                                    <li><a href="#">Website Jurusan</a></li>
                                </ul>
                                <ul class="col-sm-4">
                                    <li class="dropdown-header" id="tag-sub">Akuntansi</li>
                                    <li><a href="{{ url('/jurusan/ak') }}">Profil Jurusan</a></li>
                                    <li><a href="#">Website Jurusan</a></li>
                                    <li class="dropdown-header" id="tag-sub">APK ( Administrasi Perkantoran )</li>
                                    <li><a href="{{ url('/jurusan/apk') }}">Profil Jurusan</a></li>
                                    <li><a href="#">Website Jurusan</a></li>
                                    <li class="dropdown-header" id="tag-sub">APH ( Akomodasi Perhotelan )</li>
                                    <li><a href="{{ url('/jurusan/aph') }}">Profil Jurusan</a></li>
                                    <li><a href="#">Website Jurusan</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>
                 <li class="dropdown "><a href="#" data-toggle="dropdown" class="dropdown-toggle">Program Sekolah
                    <span><i class="fa fa-angle-down"></i></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="m-menu-content">
                                
                                <ul>
                                    <li><a href="http://sijiresik-ijotamane.com" target="_blank">Sekolah Adiwiyata</a></li>
                                    <li><a href="#">Pengembangan SMK Rujukan</a></li>
                                    <li><a href="{{ url('/lsp') }}">Lembaga Sertifikasi Profesi ( LSP )</a></li>
                                    <li><a href="#">Unit Produksi Sekolah</a></li>
                                    <li><a href="http://bkk-smkn1sby.com/" target="_blank">BKK ( Bursa Kerja Khusus )</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="dropdown "><a href="#" data-toggle="dropdown" class="dropdown-toggle">Direktori
                    <span><i class="fa fa-angle-down"></i></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="m-menu-content">
                                
                                <ul>
                                    <li><a href="#">Direktori Alumni</a></li>
                                    <li><a href="#">Info Alumni</a></li>
                                    <li><a href="#">Direktori Guru</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>
                
                
            </ul>

        </div>
        <!-- .navbar-collapse -->
    </div>
    <!-- .container -->
</nav>
<!-- .nav -->
