@include('header')
<!-- .nav -->
<section class="single-page-title">
    <div class="container text-center">
        <h2>Profil Umum</h2>
    </div>
</section>
<!-- .page-title -->

<section class="about-text ptb-100">
    <section class="section-title">
        <div class="container text-center">
        </div>
    </section>

    <div class="container">
        <div class="row">
<div class="col-md-12">
<div class="seocips-tbn-frm">
 <div class="seocips-tbn-frm_inner bottomshadows">
<iframe allowfullscreen="" frameborder="0" height="270" src="https://www.youtube.com/embed/3vlA6vv817w" width="480"></iframe>
 </div>
</div>
</div>
        </div>

        <div class="row">
<div class="profil-umum">
      <div class="col-md-3">
                <img src="assets/img/kepsek.jpg">
        </div>
        <div class="col-md-9">
        <h2>Selayang Pandang Kepala Sekolah</h2>
        <p>Era globalisasi dengan segala implikasinya menjadi salah satu pemicu cepatnya perubahan yang terjadi pada berbagai aspek kehidupan 
masyarakat, dan bila tidak ada upaya sungguh-sungguh untuk mengantisipasinya maka hal tersebut akan menjadi maslah yang sangat 
serius. Dalam hal ini dunia pendidikan mempunyai tanggung jawab yang besar, terutama dalam menyiapkan sumber daya manusia yang 
tangguh sehingga mampu hidup selaras didalam perubahan itu sendiri. Pendidikan merupakan investasi jangka panjang yang hasilnya 
tidak dapat dilihat dan dirasakan secara instan, sehingga sekolah sebagai ujung tombak dilapangan harus memiliki arah pengembangan 
jangka panjang dengan tahapan pencapaiannya yang jelas dan tetap mengakomodir tuntutan permasalahan faktual kekinian yang ada di 
masyarakat.</p>
        </div>
</div>
           

        </div><!--row-->
       <div class="row">
       <div class="col-md-12">
       <div class="profil-umum2">
<p>PEMERINTAH KOTA SURABAYA
DINAS PENDIDIKAN <br>
SMK NEGERI 1 SURABAYA <br>
JL. SMEA NO. 4,  WONOKROMO SURABAYA <br>
TELP. 031-8292038 FAX. 031- 8292039, <br>   
EMAIL : info@smkn-1.sch.id <br>
Website : http://www.smkn1-sby.sch.id <br></p>
       </div>
       </div>
       </div>
    </div>

</section>
<!-- .about-text-->

<!-- #x-corp-carousel-->
<section class="x-services ptb-100 gray-bg">

    <section class="section-title">
        <div class="container text-center">
            <h2>Berita Terbaru</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
        @foreach($rand as $key)
           <div class="col-md-3 col-sm-6">
                <div class="thumbnails thumbnail-style thumbnail-kenburn">
                    <div class="thumbnail-img">
                        <div class="overflow-hidden">
                            <img class="img-responsive" src="{{ asset($key['image']) }}" alt="">
                        </div>
                        <a class="btn-more hover-effect" href="{{ url('/detail-berita') }}/{{ $key['slug'] }}">Lihat Selengkapnya</a>
                    </div>
                    <div class="caption">
                        <h3><a class="hover-effect" href="{{ url('/detail-berita') }}/{{ $key['slug'] }}">{!! $key['judul'] !!}</a></h3>
                        <p>
                            {!! substr($key['isi'], 0,70) !!}
                        </p>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
        <!-- .row -->
    <!-- .container -->
    <!-- .container -->
</section>
<!-- .x-services -->

@include('footer')