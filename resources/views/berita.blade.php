@include('header')

<section class="single-page-title">
    <div class="container text-center">
        <h2>Berita</h2>
    </div>
</section>
<!-- .page-title -->

    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    @foreach($data as $key)
                        <div class="col-md-12">
                            <div class="head-berita">
                                <h2>
                                    <a href="{{ url('/detail-berita') }}/{{ $key['slug'] }}">{!! $key['judul'] !!}</a>
                                        &nbsp;&nbsp;
                                    {{-- <span class="label label-danger" style="color: #fff;">NEW</span> --}}
                                </h2>
                            </div>
                            <div class="konten-berita">
                                <div class="col-md-5">
                                    <img src="{{ asset($key['image']) }}" >
                                </div>
                                    <div class="col-md-7">
                                        <small>
                                            <i class="fa fa-clock-o ">&nbsp;</i> {!! $key['created_at'] !!} &nbsp; <i class="fa fa-eye">&nbsp;</i> {!! $key['dibaca'] !!} kali
                                        </small>
                                    <p>
                                        {!! substr($key['isi'], 0,125) !!}
                                    </p>
                                    <br />
                                <a href="{{ url('/detail-berita') }}/{{ $key['slug'] }}" class="btn-lihat">Lihat Selengkapnya ></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <br>
            <div class="row">
            <div class="text-center">
                <ul class="pagination">
                {!! $data->links() !!}
                </ul>
            </div>
            </div>
            </div>
        <div class="col-md-3">
        <div class="event-baru">

        <h4><i class="fa fa-calendar" style="color:#333333;">&nbsp;&nbsp;</i><a href="#" title="">Agenda Kegiatan :</a></h4><hr>
        @foreach($agenda as $key)
        <div class="sub-event">
            <div class="col-md-5">
                <img src="{{ asset($key['image']) }}" alt="{!! $key['judul'] !!}" id="event-thumb">
            </div>

            <div class="col-md-7">
                <h5><b>{!! $key['judul'] !!}</b></h5>
                <p id="tgl-thumb">{!! $key['tgl_mulai'] !!}</p>
            </div>
        </div>
        @endforeach
        <hr>
        <div style="text-align: center;">
            <a href="{{ url('/event') }}" class="event-btn">- Lihat Selengkapnya -</a>
        </div>
        
        </div>
        </div>
</div>
</div>

</section>

<!-- .about-text-->

<!-- #x-corp-carousel-->
<section class="x-services ptb-100 gray-bg">

    <section class="section-title">
        <div class="container text-center">
            <h2>Berita Terbaru</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
        @foreach($rand as $key)
           <div class="col-md-3 col-sm-6">
                <div class="thumbnails thumbnail-style thumbnail-kenburn">
                    <div class="thumbnail-img">
                        <div class="overflow-hidden">
                            <img class="img-responsive" src="{{ asset($key['image']) }}" alt="">
                        </div>
                        <a class="btn-more hover-effect" href="{{ url('/detail-berita') }}/{{ $key['slug'] }}">Lihat Selengkapnya</a>
                    </div>
                    <div class="caption">
                        <h3><a class="hover-effect" href="{{ url('/detail-berita') }}/{{ $key['slug'] }}">{!! $key['judul'] !!}</a></h3>
                        <p>{!! substr($key['isi'], 0,100) !!}</p>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
        <!-- .row -->
    <!-- .container -->
    <!-- .container -->
</section>
<!-- .x-services -->

@include('footer')