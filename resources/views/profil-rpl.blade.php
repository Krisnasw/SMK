@include('header')

<section class="single-page-title">
    <div class="container text-center">
        <h2>Profil Rekayasa Perangkat Lunak</h2>
    </div>
</section>
<!-- .page-title -->

<section class="about-text ptb-100">
    <section class="section-title">
        <div class="container text-center">
        </div>
    </section>
</section>
<div class="container">
<div class="row">
<div class="jurusan">
<div class="col-md-12">
<img src="{{ asset('assets/img/erpl.png') }}" alt="">
    <div class="seocips-tbn-frm">
 <div class="seocips-tbn-frm_inner bottomshadows">
<iframe width="480" height="270" src="{{ asset('https://www.youtube.com/embed/zqYrZPpBAAE') }}" frameborder="0" allowFullScreen=""></iframe>
 </div>
</div>
</div>
<div class="col-md-12">

<h2 style=" text-align:center; margin-top: 5%; padding: 1%;">Rekayasa Perangkat Lunak (RPL)</h2>
<p>Semakin berkembangnya teknologi informasi, dibutuhkan lulusan yang dapat mengikuti perkembangan teknologi. Oleh karena itu SMK Negeri 1 Surabaya membuka jurusan RPL sejak tahun ajaran 2006/2007 untuk memenuhi kebutuhan tenaga programmer muda di Indonesia.</p><br /><br />
</div>
</div>
</div>
<div class="row">
<div class="jurusan">
<div class="col-md-12"><br>
<div class="col-md-6">
    <h3><i class="fa fa-book">&nbsp;</i>KOMPETENSI / MATERI YANG DIAJARKAN :</h3><br><br><br><br>
    <ol>
        <li>Pemrograman Web dengan PHP</li>
        <li>Pemrograman C++ dan Visual Basic.Net</li>
        <li>Pemrograman Mobile Android</li>
        <li>Pemrograman JAVA</li>
        <li>Database MySQL dan SQL Server</li>
    </ol><br>
</div>
<div class="col-md-6">
    <h3><i class="fa fa-university">&nbsp;</i>PROFESI /BIDANG PEKERJAAN  :</h3><br><br><br><br>
    <ol>
        <li>Web Desainer</li>
        <li>Programmer Junior</li>
        <li>Implementator</li>
        <li>Technical Support</li>
        <li>Documenter Application</li>
        <li>Staff IT</li>
    </ol><br>
</div>
<div class="col-md-12">
<h3><i class="fa fa-trophy">&nbsp;</i>PRESTASI YANG DI DAPAT  :</h3><br><br><br><br>
    <ol>
        <li>Lomba Mobile Game Edukasi Nasional Juara I Tahun 2012</li>
        <li>Lomba Karya Cipta SMK Tingkat Propinsi JATIM Juara I dan III Tahun 2013</li>
        <li>Lomba Karya Cipta SMK Tingkat Propinsi JATIM Juara II Tahun 2014</li>
        <li>Lomba MAGIS CAMP “Aplikasi Bahasa Jerman berbasis Android”  Tingkat Nasional Juara 1 Tahun 2014.</li>
        <li>Lomba “Aplikasi Pelayanan Masyarakat berbasis Android”  Tingkat Kota Surabaya Juara I Tahun 2014</li>
        <li>Lomba Kompetensi Siswa (LKS) Tingkat Propinsi JATIM di Malang JUARA I Tahun 2015</li>
        <li>Lomba Semanggi Majalah Digital Tingkat Kota JUARA I  Tahun 2015</li>
        <li>Lomba Membangun Aplikasi Pemilu berbasis Android Tingkat Kota Surabaya JUARA 1 Tahun 2015</li>
        <li>Lomba Game Digital dan Majalah Digital Dinas Kominfo Kota Juara 1 dan Harapan II Tahun 2016</li>
    </ol><br>
    </div>
</div>
</div>
</div>

</div>

 <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12" >
                <h3 style="border-left: 3px solid #ff7b00; padding: 10px;">Gallery Foto</h3>
                    <div class="wow">
                    <div id="owl-works" class="owl-carousel">
                    @foreach($gal as $key)
                        <div class="item2">
                            <a href="{{ asset($key['foto_jurusan']) }}" title="{!! $key['keterangan'] !!}" data-lightbox-gallery="gallery1">
                                <img src="{{ asset($key['foto_jurusan']) }}" class="img-responsive" alt="{!! $key['keterangan'] !!}">
                            </a>
                        </div>
                    @endforeach
                    </div>
                    </div>
                </div>
            </div>
        </div><br>
    <!-- Three Columns --><br>
    <div class="container content">
        <div class="text-center margin-bottom-50">
            <h3 style="border-left: 3px solid #ff7b00; padding: 10px; text-align: left;">Produk</h3>
        </div>

            <div class="row  margin-bottom-30">
                @foreach($data as $key)
                    <div class="col-sm-4 sm-margin-bottom-30">
                        <a href="{{ asset($key['image']) }}" rel="gallery1" class="fancybox img-hover-v1" title="{!! $key['caption'] !!}">
                            <span><img class="img-responsive" src="{{ asset($key['image']) }}" alt=""><p id="text-hover">{!! $key['caption'] !!}</p></span>
                        </a>
                    </div>
                @endforeach
            </div>

    </div>
    <!-- End Three Columns -->
<!-- #x-corp-carousel-->
<section class="x-services ptb-100 gray-bg">

    <section class="section-title">
        <div class="container text-center">
            <h2>Berita Terbaru</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
        @foreach($rand as $key)
           <div class="col-md-3 col-sm-6">
                <div class="thumbnails thumbnail-style thumbnail-kenburn">
                    <div class="thumbnail-img">
                        <div class="overflow-hidden">
                            <img class="img-responsive" src="{{ asset($key['image']) }}" alt="">
                        </div>
                        <a class="btn-more hover-effect" href="{{ url('/detail-berita') }}/{{ $key['slug'] }}">Lihat Selengkapnya</a>
                    </div>
                    <div class="caption">
                        <h3><a class="hover-effect" href="{{ url('/detail-berita') }}/{{ $key['slug'] }}">{!! $key['judul'] !!}</a></h3>
                        <p>{!! substr($key['isi'], 0,100) !!}</p>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
        <!-- .row -->
    <!-- .container -->
    <!-- .container -->
</section>

@include('footer')