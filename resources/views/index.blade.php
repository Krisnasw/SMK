@include('header')

<div id="x-corp-carousel" class="carousel slide hero-slide hidden-xs" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#x-corp-carousel" data-slide-to="0" class="active"></li>
        <li data-target="#x-corp-carousel" data-slide-to="1"></li>
        <li data-target="#x-corp-carousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox" id="ggwp">
    @foreach($slider as $key)
        <div class="item">
            <img src="{{ asset($key['image']) }}" alt="{!! $key['slug'] !!}">
        </div>
    @endforeach
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#x-corp-carousel" role="button" data-slide="prev">
        <i class="fa fa-angle-left" aria-hidden="true"></i>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#x-corp-carousel" role="button" data-slide="next">
        <i class="fa fa-angle-right" aria-hidden="true"></i>
        <span class="sr-only">Next</span>
    </a>
</div>  

<section class="x-features">
    <section class="section-title">
        <div class="container text-center">
            <h2>MAKLUMAT PELAYANAN</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
            <br>
        <div class="row">
        
            <div class="col-md-4 img-left">
                <img id="#" class="img-responsive" src="assets/img/maklumat.png" alt="Logo SMKN 1 Surabaya">
            </div>
            <div class="maklumat">

            <div class="col-md-8">
                <div class="promo-block-wrapper clearfix">
                    <div class="promo-icon">
                       
                    </div>
                    <div class="promo-content">
                       <h3>Menyelenggarakan pelayanan publik di bidang pendidikan dengan standar 
yang telah ditetapkan secara konsisten, akuntabel dan berkelanjutan.</h3>


                    </div>
                </div>
                <!-- /.promo-block-wrapper -->

                <div class="promo-block-wrapper clearfix">
                    <div class="promo-icon">
                 
                    </div>
                    <div class="promo-content">
                        <h3>Memperhatikan keluhan masyarakat dengan menerima kritik, menangani 
pengaduan dan menindaklanjutinya secara cepat dan tuntas.</h3>

                    </div>
                </div>
                <!-- /.promo-block-wrapper -->

                <div class="promo-block-wrapper clearfix">
                    <div class="promo-icon">
                       </div>
                    
                    <div class="promo-content last-type">
                        <h3>Mengoptimalkan pemanfaatan teknologi informasi dan melakukan inovasi 
secara terus menerus untuk peningkatan kualitas pelayanan publik.</h3>

                    </div>
                </div>
        </div>

            </div>
    </section> 
</section>
                <!-- /.promo-block-wrapper -->

<!-- Service Section-->

<!-- #x-corp-carousel-->
<section class="x-services ptb-100 gray-bg">

    <section class="section-title">
        <div class="container text-center">
            <h2>Berita Terbaru</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
        @foreach($rand as $key)
           <div class="col-md-3 col-sm-6">
                <div class="thumbnails thumbnail-style thumbnail-kenburn">
                    <div class="thumbnail-img">
                        <div class="overflow-hidden">
                            <img class="img-responsive" src="{{ asset($key['image']) }}" alt="">
                        </div>
                        <a class="btn-more hover-effect" href="{{ url('/detail-berita') }}/{{ $key['slug'] }}">Lihat Selengkapnya</a>
                    </div>
                    <div class="caption">
                        <h3><a class="hover-effect" href="{{ url('/detail-berita') }}/{{ $key['slug'] }}">{!! $key['judul'] !!}</a></h3>
                        <p>
                        {!! substr($key['isi'], 0,70) !!}
                        </p>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
        <!-- .row -->
    <!-- .container -->
    <!-- .container -->
</section>
<!-- .x-services -->

<section class="x-features">
    <section class="section-title">
        <div class="container text-center">
            <h2>LOKASI SMK NEGERI 1 SURABAYA</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
            </div>
            <br>
     <div class="container">
        <div class="row">
        <div class="about-us">
            <div class="col-md-4">
                <h3>Alamat</h3>
                <p>JL.SMEA NO. 4 , WONOKROMO</p>
                <p>SURABAYA 60111, INDONESIA</p>

                <h3>Kontak</h3>
                <p>TELP : 031-8292038</p>
                <p>FAX   : 031- 8292039 </p>
                <p>EMAIL : <a href="mailto:info@smkn-1.sch.id">info@smkn-1.sch.id</a></p>
            </div>
            <div class="col-md-8">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15829.731345278906!2d112.7343453!3d-7.3051724!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5959ad479fd46d02!2sSMK+Negeri+1+Surabaya!5e0!3m2!1sid!2sid!4v1470964777302" width="100%" height="420px" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
        </div>
        <!-- /.row -->
    </div>
        
    </section> 
</section>

<section id="services" class="services text-center">
    <div class="section-padding">
      <div class="container">
        <div class="row">
          <div class="section-top wow animated fadeInUp" data-wow-delay=".5s">
            <h2 class="section-title"><span>Special</span>&nbsp;Thanks</h2>
          </div><!-- /.section-top -->

          <div class="section-details">
            <div class="service-details">

            @foreach($link as $key)
              <div class="col-md-3 col-sm-6">
                <div class="item wow animated fadeInLeft" data-wow-delay=".5s">
                  <div class="item-icon">
                    <a href="{!! $key['link'] !!}">
                      <img src="{{ asset($key['image']) }}" alt="{!! $key['keterangan'] !!}">
                    </a>
                  </div><!-- /.item-icon -->
                  <div class="item-details">

                  </div><!-- /.item-details -->
                </div><!-- /.item -->
              </div>
            @endforeach

            </div><!-- /.service-details -->
          </div><!-- /.section-details -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </div><!-- /.section-padding -->
  </section><!-- /#services -->

  <!-- Service Section-->   

@include('footer')