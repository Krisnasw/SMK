@include('header')

<section class="single-page-title">
    <div class="container text-center">
        <h2>Ekstrakurikuler</h2>
    </div>
</section>
<!-- .page-title -->

<section class="about-text ptb-100">
    <section class="section-title">
        <div class="container text-center">
        </div>
    </section>
</section>
<div class="container">
<div class="row">
<h2 style=" text-align:left; padding: 1%; border-left: 5px solid #000; font-size: 20px;">Ekstrakurikuler SMK Negeri 1 Surabaya</h2><br>
<div class="col-md-12 col-sm-12 col-xs-12">

    <table class="table table-striped">
    <thead>
      <tr>
        <th>Ekstrakurikuler</th>
        <th>Waktu Pelaksanaan</th>
        <th>Pengurus</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Sie Kerohanian Islam ( SKI )</td>
        <td>Jum'at</td>
        <td>-</td>
      </tr>
      <tr>
        <td>Hadrah</td>
        <td>Senin</td>
        <td>-</td>
      </tr>
      <tr>
        <td>Kharisma</td>
        <td>Selasa</td>
        <td>-</td>
      </tr>
       <tr>
        <td>Tartil</td>
        <td>Selasa</td>
        <td>-</td>
      </tr>
      <tr>
        <td>Sie kerohanian Kristen ( SKK )</td>
        <td>Selasa</td>
        <td>-</td>
      </tr>
      <tr>
        <td>Tartil</td>
        <td>Selasa</td>
        <td>-</td>
      </tr>
        <tr>
        <td>Paskanisa ( Paskibraka SMK Negeri 1 Surabaya )</td>
        <td>Selasa</td>
        <td>-</td>
      </tr>
      <tr>
        <td>Palang Merah Remaja ( PMR )</td>
        <td>Selasa</td>
        <td>-</td>
      <tr>
        <td>SBLH</td>
        <td>Selasa</td>
        <td>-</td>
      </tr>
      </tr>
      <tr>
        <td>Pramuka</td>
        <td>Selasa</td>
        <td>-</td>
      </tr>
      <tr>
        <td>Jurnalis</td>
        <td>Selasa</td>
        <td>-</td>
      </tr>
      <tr>
        <td>Basket</td>
        <td>Selasa</td>
        <td>-</td>
      </tr>
      <tr>
        <td>Voli</td>
        <td>Selasa</td>
        <td>-</td>
      </tr>
      <tr>
        <td>Sepak Takraw</td>
        <td>Selasa</td>
        <td>-</td>
      </tr>
      <tr>
        <td>Futsal</td>
        <td>Selasa</td>
        <td>-</td>
      </tr>
      <tr>
        <td>Karate</td>
        <td>Selasa</td>
        <td>-</td>
      </tr>
      <tr>
        <td>Karawitan</td>
        <td>Selasa</td>
        <td>-</td>
      </tr>
      <tr>
        <td>Teater</td>
        <td>Selasa</td>
        <td>-</td>
      </tr>
      <tr>
        <td>Dance</td>
        <td>Selasa</td>
        <td>-</td>
      </tr>
      <tr>
        <td>Band</td>
        <td>Selasa</td>
        <td>-</td>
      </tr>
      <tr>
        <td>Club IT</td>
        <td>Selasa</td>
        <td>-</td>
      </tr>
      <tr>
        <td>Debate B. Inggris</td>
        <td>Selasa</td>
        <td>-</td>
      </tr>
      <tr>
        <td>B. Jerman</td>
        <td>Selasa</td>
        <td>-</td>
      </tr>
    </tbody>
  </table>
</div>
</div>
</div>
<!-- #x-corp-carousel-->
<section class="x-services ptb-100 gray-bg">

    <section class="section-title">
        <div class="container text-center">
            <h2>Berita Terbaru</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
        @foreach($rand as $key)
           <div class="col-md-3 col-sm-6">
                <div class="thumbnails thumbnail-style thumbnail-kenburn">
                    <div class="thumbnail-img">
                        <div class="overflow-hidden">
                            <img class="img-responsive" src="{{ asset($key['image']) }}" alt="">
                        </div>
                        <a class="btn-more hover-effect" href="{{ url('/detail-berita') }}/{{ $key['slug'] }}">Lihat Selengkapnya</a>
                    </div>
                    <div class="caption">
                        <h3><a class="hover-effect" href="{{ url('/detail-berita') }}/{{ $key['slug'] }}">{!! $key['judul'] !!}</a></h3>
                        <p>
                            {!! substr($key['isi'], 0,70) !!}
                        </p>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
        <!-- .row -->
    <!-- .container -->
    <!-- .container -->
</section>

@include('footer')