@include('header')

<section class="single-page-title">
    <div class="container text-center">
        <h2>Profil Akuntansi</h2>
    </div>
</section>
<!-- .page-title -->

<section class="about-text ptb-100">
    <section class="section-title">
        <div class="container text-center">
        </div>
    </section>
</section>
<div class="container">
<div class="row">
<div class="jurusan">
<div class="col-md-12">
<!-- <img src="assets/img/dkv/dkv.png" alt=""> --><hr>
    <!-- <div class="seocips-tbn-frm">
 <div class="seocips-tbn-frm_inner bottomshadows">
<iframe width="480" height="270" src="https://www.youtube.com/embed/zqYrZPpBAAE" frameborder="0" allowFullScreen=""></iframe>
 </div>
</div> -->
</div>
<div class="col-md-12">

<h2 style=" text-align:center; margin-top: 5%; padding: 1%;">Akuntansi (AK)</h2><br>
<p>Kompetensi keahlian akuntansi di SMK Negeri 1 Surabaya meliputi pembelajaran Akuntansi Manual dan Akutansi Komputer (MYOB). Kompetensi Keahlian Akuntansi di SMK Negeri 1 Surabaya bertujuan agar siswa dapat mengetahui Akuntansi baik untuk perusahaan jasa, perusahaan dagang, dan perusahaan manufaktur. Siswa kompetensi Keahlian Akuntansi diharapkan dapat melakukan Siklus Akuntansi minimal bagi dirinya sendiri dan perusahaan pada umumnya dan sekaligus mampu menerapkan Sistem Perpajakan di Indonesia<br><br>
Lulusan Kompetensi Keahlian Akuntansi SMK Negeri 1 Surabaya , ada yang kuliah , bekerja dan tidak sedikit yang melanjutkan kuliah sambil bekerja.  Untuk Kompetensi Keahlian Akuntansi belajar mengenai Siklus Akuntansi, maka tidak ada karya yang bersifat riil atau produk nyata yang bisa di pamerkan. Tapi Laporan Keuangan merupakan produk jasa akuntansi

</p>
</div>
</div>
</div>
<br><br>
<div class="row">
<div class="jurusan">
<div class="col-md-12"><hr><br>
<div class="col-md-6">
    <h3><i class="fa fa-book">&nbsp;</i>KOMPETENSI / MATERI YANG DIAJARKAN :</h3><br><br><br><br>
    <ol>
        <li>Pengantar Ekonomi dan Bisnis</li>
        <li>Pengantar Administrasi Perkantoran  </li>
        <li>Akuntansi Keuangan</li>
        <li>Akuntansi Perusahaan Dagang</li>
        <li>Akuntansi Manufaktur</li>
        <li>Komputer Akuntansi</li>
        <li>Administrasi Pajak</li>
    </ol><br>
</div>
<div class="col-md-6">
    <h3><i class="fa fa-university">&nbsp;</i>PROFESI /BIDANG PEKERJAAN  :</h3><br><br><br><br>
    <ol>
        <li>Penata Buku Muda dalam lingkup akuntansi</li>
        <li>Kasir / Teller</li>
        <li>Juru Penggajian</li>
        <li>Operator Mesin Hitung</li>
        <li>Administrasi Gudang</li>
        <li>Menyusun Laporan Keuangan</li>
        <li>DLL</li>
    </ol><br>
</div>
<div class="col-md-12">
<h3><i class="fa fa-trophy">&nbsp;</i>PRESTASI YANG DI DAPAT  :</h3><br><br><br><br>
    <ol>
        <li>Juara 3 Lomba Akuntansi di Univ Widya Mandala Surabaya</li>
        <li>Juara 1 SAC di Univ Wijaya Putra Surabaya 2013</li>
        <li>Juara 2 SAC di Univ Wijaya Putra Surabaya 2013</li>
        <li>Juara 3 Lomba Akuntansi di Univ Widya Mandala Surabaya 2014</li>
        <li>Juara 1 SAC di Univ Wijaya Putra Surabaya 2014</li>
        <li>Juara 1 Akuntansi Myob di STIE Perbanas Surabaya </li>
        <li>Juara 1 LKS Tingat kota Surabaya 2015</li>
        <li>Juara 1 SAC di Univ Wijaya Putra Surabaya 2015</li>
        <li>Juara 4 SAC di Univ Wijaya Putra Surabaya 2015</li>
        <li>Juara 2  Accounting Competition di UNESA Surabaya</li>
        <li>Juara 2 Lomba Akuntansi di UNAIR 2016</li>
        <li>Juara 1 Accounting Competition di UNESA Surabaya 2016</li>
    </ol><br>
    </div>
</div>
</div>
</div>

</div>

 <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12" >
                <h3 style="border-left: 3px solid #ff7b00; padding: 10px;">Gallery Foto</h3>
                    <div class="wow">
                    <div id="owl-works" class="owl-carousel">
                    @foreach($gal as $key)
                        <div class="item2">
                            <a href="{{ asset($key['foto_jurusan']) }}" title="{!! $key['keterangan'] !!}" data-lightbox-gallery="gallery1">
                                <img src="{{ asset($key['foto_jurusan']) }}" class="img-responsive" alt="{!! $key['keterangan'] !!}">
                            </a>
                        </div>
                    @endforeach
                    </div>
                    </div>
                </div>
            </div>
        </div><br>
    <!-- Three Columns --><br>
    <div class="container content">
        <div class="text-center margin-bottom-50">
            <h3 style="border-left: 3px solid #ff7b00; padding: 10px; text-align: left;">Produk</h3>
        </div>

            <div class="row  margin-bottom-30">
                @foreach($data as $key)
                    <div class="col-sm-4 sm-margin-bottom-30">
                        <a href="{{ asset($key['image']) }}" rel="gallery1" class="fancybox img-hover-v1" title="{!! $key['caption'] !!}">
                            <span><img class="img-responsive" src="{{ asset($key['image']) }}" alt=""><p id="text-hover">{!! $key['caption'] !!}</p></span>
                        </a>
                    </div>
                @endforeach
            </div>

    </div>
    <!-- End Three Columns -->
<!-- #x-corp-carousel-->
<section class="x-services ptb-100 gray-bg">

    <section class="section-title">
        <div class="container text-center">
            <h2>Berita Terbaru</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
        @foreach($rand as $key)
           <div class="col-md-3 col-sm-6">
                <div class="thumbnails thumbnail-style thumbnail-kenburn">
                    <div class="thumbnail-img">
                        <div class="overflow-hidden">
                            <img class="img-responsive" src="{{ asset($key['image']) }}" alt="">
                        </div>
                        <a class="btn-more hover-effect" href="{{ url('/detail-berita') }}/{{ $key['slug'] }}">Lihat Selengkapnya</a>
                    </div>
                    <div class="caption">
                        <h3><a class="hover-effect" href="{{ url('/detail-berita') }}/{{ $key['slug'] }}">{!! $key['judul'] !!}</a></h3>
                        <p>{!! substr($key['isi'], 0,100) !!}</p>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
        <!-- .row -->
    <!-- .container -->
    <!-- .container -->
</section>

@include('footer')