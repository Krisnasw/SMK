<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="{!! asset(Auth::user()->image) !!}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>{!! Auth::user()->name !!}</p>
              <a href="#"><i class="fa fa-circle text-success"></i>Dibuat : {!! date('d-M-y', strtotime(Auth::user()->created_at)) !!}</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
              <a href="{!! url('/smeas-admin/home') !!}">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>
            <li class="treeview">
              <a href="{{ url('/smeas-admin/slider') }}">
                <i class="fa fa-image"></i> <span>Slider</span>
              </a>
            </li>
            <li class="treeview">
              <a href="{{ url('/smeas-admin/gallery') }}">
                <i class="fa fa-clone"></i> <span>Gallery Jurusan</span>
              </a>
            </li>
            <li class="treeview">
              <a href="{{ url('/smeas-admin/produk') }}">
                <i class="fa fa-database"></i> <span>Produk Jurusan</span>
              </a>
            </li>
            {{-- <li class="treeview">
              <a href="{{ url('/smeas-admin/setting') }}">
                <i class="fa fa-cog"></i> <span>Setting Jurusan</span>
              </a>
            </li> --}}
            <li class="treeview">
              <a href="{{ url('/smeas-admin/berita') }}">
                <i class="fa fa-feed"></i> <span>Berita</span>
              </a>
            </li>
            <li class="treeview">
              <a href="{{ url('/smeas-admin/agenda') }}">
                <i class="fa fa-calendar"></i> <span>Agenda</span>
              </a>
            </li>
            <li class="treeview">
              <a href="{{ url('/smeas-admin/link') }}">
                <i class="fa fa-link"></i> <span>Link Terkait</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>