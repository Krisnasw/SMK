<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Administrator | Produk Jurusan</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    {!! Html::style('assets/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! Html::style('assets/css/font-awesome.min.css') !!}
    <!-- Ionicons -->
    {!! Html::style('assets/css/ionicons.min.css') !!}
    <!-- Theme style -->
    {!! Html::style('dist/css/AdminLTE.min.css') !!}
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    {!! Html::style('dist/css/skins/_all-skins.min.css') !!}

    {!! Html::style('plugins/datatables/dataTables.bootstrap.css') !!}

    {!! Html::style('plugins/select2/select2.min.css') !!}
    {!! Html::style('assets/css/sweetalert.css') !!}
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      @include('admin.header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Administrator - Produk Jurusan
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/smeas-admin/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Produk Jurusan</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6" style="margin-left: 25%;">

              <!-- Input addon -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Tambahkan Produk Jurusan</h3>
                </div>
                {!!  Form::open(array('action' => 'ProdukController@store', 'method' => 'POST', 'files' => true)) !!}
                <div class="box-body">
                  <div class="input-group">
                    <div class="input-group-btn">
                      <button type="button" class="btn btn-danger">Gambar</button>
                    </div><!-- /btn-group -->
                    <input type="file" class="form-control" name="image">
                  </div><!-- /input-group -->
                  <br />
                  <div class="input-group">
                    <div class="input-group-btn">
                      <button type="button" class="btn btn-info">Caption</button>
                    </div>
                    <input type="text" class="form-control" name="keterangan">
                  </div>
                  <br />
                  <div class="form-group">
                    <select class="form-control select2" style="width: 100%; text-align: center;" name="jurusan">
                      <option value="APK">Administrasi Perkantoran</option>
                      <option value="AK">Akuntansi</option>
                      <option value="RPL">Rekayasa Perangkat Lunak</option>
                      <option value="MM">Multimedia</option>
                      <option value="TKJ">Teknik Komputer dan Jaringan</option>
                      <option value="DKV">Desain Komunikasi Visual</option>
                      <option value="PBR">Pemasaran</option>
                      <option value="APH">Akomodasi Perhotelan</option>
                      <option value="BC">Broadcasting</option>
                    </select>
                  </div><!-- /.form-group -->
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="button" onclick="reset();" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info pull-right">Submit</button>
                </div><!-- /.box-footer -->
                {!!  Form::close() !!}
              </div><!-- /.box -->

            </div><!--/.col (left) -->
            <!-- right column -->
          </div>   <!-- /.row -->

          <div class="row">
          <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Data Produk Jurusan</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th width="10%;">Gambar</th>
                        <th>Slug</th>
                        <th>Caption</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php $no = 0; ?>
                    @foreach($data as $key)
                      <tr>
                        <td>{{ ++$no }}</td>
                        <td><img src="{{ asset($key['image']) }}" class="img img-responsive"></td>
                        <td>{!! $key['jurusan'] !!}</td>
                        <td>{!! $key['caption'] !!}</td>
                        <td>
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#show{{{ $key['id'] }}}"><i class="fa fa-eye"></i></button>
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#edit{{{ $key['id'] }}}"><i class="fa fa-pencil"></i></button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete{{{ $key['id'] }}}"><i class="fa fa-trash"></i></button>
                        </td>
                      </tr>
                            <div id="edit{{{ $key['id'] }}}" class="modal fade">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header bg-warning">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h6 class="modal-title">Edit Data</h6>
                                  </div>
                                  {!! Form::open(array('files' => true, 'method' => 'PATCH', 'route' => array('produk.update', $key['id']))) !!}
                                  <div class="modal-body">
                                    <div class="form-group">
                                      <select class="form-control select2" style="width: 100%; text-align: center;" name="jurusan">
                                        <option selected>-- Jurusan --</option>
                                        <option value="AK">Akuntansi</option>
                                        <option value="APK">Administrasi Perkantoran</option>
                                        <option value="PBR">Pemasaran</option>
                                        <option value="APH">Akomodasi Perhotelan</option>
                                        <option value="RPL">Rekayasa Perangkat Lunak</option>
                                        <option value="MM">Multimedia</option>
                                        <option value="DKV">Desain Komunikasi Visual</option>
                                        <option value="TKJ">Teknik Komputer dan Jaringan</option>
                                        <option value="BC">Broadcasting</option>
                                      </select>
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                          <div class="fileinput-new thumbnail" style="width: 200px;">
                                          @if(empty($key['image']))
                                            <img src="{{ asset('images/cabang/cabang.jpg') }}" alt=""/>
                                          @else
                                            <img src="{{ asset($key['image']) }}" alt=""/>
                                          @endif
                                          </div>
                                          <div class="input-group">
                                            <div class="input-group-btn">
                                              <button type="button" class="btn btn-danger">Gambar</button>
                                            </div><!-- /btn-group -->
                                            <input type="file" class="form-control" name="image">
                                          </div><!-- /input-group -->
                                          <br />
                                          <div class="input-group">
                                            <div class="input-group-btn">
                                              <button type="button" class="btn btn-info">Caption</button>
                                            </div>
                                            <input type="text" class="form-control" value="{!! $key['caption'] !!}" name="keterangan">
                                          </div>
                                        </div>
                                    </div>
                                  </div>

                                  <div class="modal-footer">
                                  {!! Form::submit("Ya", array('class' => 'btn btn-danger')) !!}
                                    <button type="button" class="btn btn-link" data-dismiss="modal">Tidak</button>
                                  </div>
                                  {!! Form::close() !!}
                                </div>
                              </div>
                            </div>
                            <div id="delete{{{ $key['id'] }}}" class="modal fade">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header bg-danger">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h6 class="modal-title">Hapus Data</h6>
                                  </div>

                                  <div class="modal-body">
                                    <h6 class="text-semibold">Apakah Anda Yakin Ingin Menghapus, <i> {!! $key['caption'] !!} </i></h6>
                                    <p></p>
                                    <p>NB : Data tidak dapat dikembalikan jika sudah dihapus.</p>
                                  </div>

                                  <div class="modal-footer">
                                    {!! Form::open(array('method' => 'DELETE', 'route' => array('produk.destroy', $key['id']))) !!}
                                      {!! Form::submit("Ya", array('class' => 'btn btn-danger')) !!}
                                    {!! Form::close() !!}
                                    <button type="button" class="btn btn-link" data-dismiss="modal">Tidak</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div id="show{{{ $key['id'] }}}" class="modal fade">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header bg-primary">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h6 class="modal-title">Lihat Data</h6>
                                  </div>

                                  <div class="modal-body">
                                    <div class='box-body'>
                                      <img class="img-responsive pad" src="{{ asset($key['image']) }}" alt="Photo">
                                    </div><!-- /.box-body -->
                                  </div>

                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                    @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>No</th>
                        <th>Gambar</th>
                        <th>Slug</th>
                        <th>Caption</th>
                        <th>Aksi</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
          </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      @include('admin.footer')

    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    {!! Html::script('plugins/jQuery/jQuery-2.1.4.min.js') !!}
    <!-- Bootstrap 3.3.5 -->
    {!! Html::script('assets/js/bootstrap.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('plugins/fastclick/fastclick.min.js') !!}

    {!! Html::script('plugins/datatables/jquery.dataTables.min.js') !!}
    {!! Html::script('plugins/datatables/dataTables.bootstrap.min.js') !!}
    <!-- AdminLTE App -->
    {!! Html::script('dist/js/app.min.js') !!}
    <!-- AdminLTE for demo purposes -->
    {!! Html::script('dist/js/demo.js') !!}
    {!! Html::script('plugins/select2/select2.full.min.js') !!}
    {!! Html::script('plugins/slimScroll/jquery.slimscroll.min.js') !!}
    {!! Html::script('assets/js/sweetalert.min.js') !!}
    @include('sweet::alert')
    <script>
      $(function () {
        $("#example1").DataTable();
        $(".select2").select2();
      });
    </script>
  </body>
  </body>
</html>
