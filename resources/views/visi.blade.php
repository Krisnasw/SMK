@include('header')

<section class="single-page-title">
    <div class="container text-center">
        <h2>Visi Dan Misi</h2>
    </div>
</section>
<!-- .page-title -->

   <section class="about-text ptb-100">
        <section class="section-title">
            <div class="container">
            <div class="noob">
                <h3>Visi</h3>
                <p>MENJADI SEKOLAH MENENGAH KEJURUAN BERSTANDAR INTERNASIONAL YANG TAMATANNYA PROFESIONAL, BERBUDI PEKERTI LUHUR, BERWAWASAN LINGKUNGAN SERTA MAMPU BERKOMPETISI DI ERA GLOBAL.</p>
            </div> 
            </div>
        </section>
        <section class="section-title">
            <div class="container">
            <div class="noob">
                <h3>Misi</h3>
                <p><ol>
                    <li>MENERAPKAN MANAJEMEN STANDAR ISO DALAM PENGELOLAAN SEKOLAH.</li>
                    <li>MENINGKATKAN PROFESIONALISME.</li>
                    <li>MENINGKATKAN MUTU PENYELENGGARAAN PENDIDIKAN.</li>
                    <li>MEMBANGUN SERTA MEMBERDAYAKAN SMK BERTARAF INTERNASIONAL SEHINGGA MENGHASILKAN LULUSAN YANG MEMILIKI JATI DIRI BANGSA DAN KEUNGGULAN KOMPETITIF DI PASAR NASIONAL DAN GLOBAL.</li>
                    <li>MENGEMBANGKAN KERJASAMA INDUSTRI, BERSKALA NASIONAL MAUPUN INTERNASIONAL</li>
                </ol></p>
            </div>
            </div>
        </section>
        <section class="section-title">
            <div class="container">
            <div class="noob">
                <h3>Motto</h3>
                <p>“PELAYANAN PROFESIONAL KEPUASAN PELANGGAN”</p>
            </div>
        </section>
        <section class="section-title">
            <div class="container">
            <div class="noob">
                <h3>Kebijakan Mutu</h3>
                <p>SMK NEGERI 1 SURABAYA BERTEKAD MENCAPAI PERBAIKAN YANG BERKESINAMBUNGAN
    BERDASARKAN SISTEM MANAJEMEN MUTU ISO 9001 : 2000
    DENGAN MEMBERIKAN PELAYANAN PENDIDIKAN KEJURUAN YANG PROFESSIONAL UNTUK</p>
    <p>
        <ol>
            <li>MENINGKATKAN MORAL & DISIPLIN SISWA</li>
            <li>MENINGKATKAN PENGETAHUAN, KETERAMPILAN & KEAHLIAN SISWA YANG MAMPU BERSAING SECARA GLOBAL</li>
            <li>MENINGKATKAN SISTEM PENGAJARAN YANG TERSTRUKTUR DAN TERSTANDARISASI</li>
            <li>MEMBEKALI KECAKAPAN HIDUP YANG BERMANFAAT DALAM MEMENUHI HARAPAN PIHAK – PIHAK TERKAIT DAN PERATURAN PERUNDANG – UNDANGAN DENGAN MENYEDIAKAN SARANA PRASARANA SERTA MENINGKATKAN SUMBER DAYA MANUSIA YANG TANGGAP TERHADAP PERKEMBANGAN TEKNOLOGI</li>
        </ol>
    </p>
            </div>
            </div>
            </section>
        </section>
<!-- .about-text-->

<!-- #x-corp-carousel-->
<section class="x-services ptb-100 gray-bg">

    <section class="section-title">
        <div class="container text-center">
            <h2>Berita Terbaru</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
        @foreach($rand as $key)
           <div class="col-md-3 col-sm-6">
                <div class="thumbnails thumbnail-style thumbnail-kenburn">
                    <div class="thumbnail-img">
                        <div class="overflow-hidden">
                            <img class="img-responsive" src="{{ asset($key['image']) }}" alt="">
                        </div>
                        <a class="btn-more hover-effect" href="{{ url('/detail-berita') }}/{{ $key['slug'] }}">Lihat Selengkapnya</a>
                    </div>
                    <div class="caption">
                        <h3><a class="hover-effect" href="{{ url('/detail-berita') }}/{{ $key['slug'] }}">{!! $key['judul'] !!}</a></h3>
                        <p>
                            {!! substr($key['isi'], 0,70) !!}
                        </p>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
        <!-- .row -->
    <!-- .container -->
    <!-- .container -->
</section>
<!-- .x-services -->
  <!-- Service Section-->   
@include('footer')