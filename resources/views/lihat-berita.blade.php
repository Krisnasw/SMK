@include('header')

<section class="single-page-title">
    <div class="container text-center">
    </div>
</section>
<!-- .page-title -->

    <div class="container">
        <div class="row">
        <!-- Mulai Detail -->
    @foreach($data as $key)
        <div class="col-md-9">
            <div class="lihat-berita">
                <h3>{!! $key['judul'] !!}</h3>
                <p> 
                    <i class="fa fa-clock-o ">&nbsp;</i> {!! $key['created_at'] !!} &nbsp; 
                    <i class="fa fa-eye">&nbsp;</i> {!! $key['dibaca'] !!} kali
                </p>
            <div class="slider">
                <img src="{{ asset($key['image']) }}"/>
            </div>
            <br>
            <br>
            <br>
            <p>
                {!! $key['isi'] !!}
            </p>
            </div>
            <div class="row">
                <div class="discuss">
                    <div id="disqus_thread"></div>
                </div>
            </div>
        </div>
    @endforeach
        <!-- End Detail -->
        <div class="col-md-3">
        <div class="event-baru">

        <h4><i class="fa fa-calendar" style="color:#333333;">&nbsp;&nbsp;</i><a href="#" title="">Agenda Kegiatan :</a></h4><hr>
        @foreach($agenda as $key)
        <div class="sub-event">
            <div class="col-md-5">
                <img src="{{ asset($key['image']) }}" alt="{!! $key['judul'] !!}" id="event-thumb">
            </div>

            <div class="col-md-7">
                <h5><b>{!! $key['judul'] !!}</b></h5>
                <p id="tgl-thumb">{!! $key['tgl_mulai'] !!}</p>
            </div>
        </div>
        @endforeach

        <hr>
        <div style="text-align: center;">
            <a href="event.php" class="event-btn">- Lihat Selengkapnya -</a>
        </div>
        
        </div>
        </div>

</div>
</div>
</section>
<!-- .about-text-->
<!-- #x-corp-carousel-->

<section class="x-services ptb-100 gray-bg">

    <section class="section-title">
        <div class="container text-center">
            <h2>Berita Terbaru</h2>
            <span class="bordered-icon"><i class="fa fa-circle-thin"></i></span>
        </div>
    </section>

    <div class="container">
        <div class="row">
        @foreach($rand as $key)
           <div class="col-md-3 col-sm-6">
                <div class="thumbnails thumbnail-style thumbnail-kenburn">
                    <div class="thumbnail-img">
                        <div class="overflow-hidden">
                            <img class="img-responsive" src="{{ asset($key['image']) }}" alt="">
                        </div>
                        <a class="btn-more hover-effect" href="{{ url('/detail-berita') }}/{{ $key['slug'] }}">Lihat Selengkapnya</a>
                    </div>
                    <div class="caption">
                        <h3><a class="hover-effect" href="{{ url('/detail-berita') }}/{{ $key['slug'] }}">{!! $key['judul'] !!}</a></h3>
                        <p>{!! substr($key['isi'], 0,100) !!}</p>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
        <!-- .row -->
    <!-- .container -->
    <!-- .container -->
</section>
<!-- .x-services -->
@include('footer')